﻿using ImageProcessing;
using SkiaSharp;

var s = new VinylSpiral()
{
    LineSpacing = 0.1f,
    LineWidth = 0.1f,
    StartPosition = new System.Numerics.Vector2(-1, 0)
};

var image = SKBitmap.Decode("./Test.jpg");
for (int i = 0; i < image.Width; i++)
{
    Console.WriteLine();
    Console.WriteLine("{0} + ", VinylDecoder.GetPixelBrightness(image, i, image.Height / 2));
    Console.WriteLine("{0} - ", VinylDecoder.SamplePoint(image, i, 0.5f + image.Height / 2.0f));
    Console.WriteLine("{0} + ", VinylDecoder.GetPixelBrightness(image, i, 1+image.Height / 2));
    Console.WriteLine();
}