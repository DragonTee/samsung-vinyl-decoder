using System.Numerics;
using SkiaSharp;

namespace ImageProcessing;
public class VinylDecoder
{
    public static IReadOnlyList<float> Decode(SKBitmap bitmap)
    {


        return null;
    }

    public static VinylSpiral GetVinylSpiral(SKBitmap bitmap)
    {
        return null;
    }

    private static VinylSpiral SpiralBacktracking(SKBitmap bitmap, VinylSpiral spiral)
    {
        return null;
    }

    

    public static float SamplePoint(SKBitmap bitmap, float x, float y)
    {
        var x1 = ((float)Math.Truncate(x));
        var x2 = ((float)Math.Floor(x+1));
        var y1 = ((float)Math.Truncate(y));
        var y2 = ((float)Math.Floor(y+1));
        var exactPoint = new Vector2(x, y);
        var points = new List<Vector2>
        {
            new Vector2(x1, y1),
            new Vector2(x2, y1),
            new Vector2(x1, y2),
            new Vector2(x2, y2),
        };
        var distances = new List<float>();
        foreach (var point in points)
        {
            distances.Add(Vector2.Distance(exactPoint, point));
        }
        distances = distances.Select(x => 1 / (x + 0.000001f)).ToList();
        distances = distances.Select(x => x / distances.Sum()).ToList();
        var result = 0f;
        points.Zip(distances).ToList().ForEach(x =>
        {
            result += GetPixelBrightness(bitmap, (int)x.First.X, (int)x.First.Y) * (x.Second);
        }
        );
        return result;
    }

    public static float GetPixelBrightness(SKBitmap bitmap, int x, int y)
    {
        var color = bitmap.GetPixel(x, y);
        return (color.Red + color.Green + color.Blue) / 3;
    }
}