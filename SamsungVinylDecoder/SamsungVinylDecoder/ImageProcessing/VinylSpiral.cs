using System.Numerics;
using SkiaSharp;

namespace ImageProcessing;
public class VinylSpiral
{
    public enum SpiralDirection
    {
        Clockwise,
        CounterClockwise,
    }
    public float LineWidth{ get; set; }
    public float LineSpacing { get; set; }
    public SpiralDirection Direction { get; set; } = SpiralDirection.CounterClockwise;

    /// <summary>
    /// Position of a starting point, relative to the center of a spiral
    /// </summary>
    public Vector2 StartPosition { get; set; }

    public Vector2 GetPoint(float time, float speed)
    {
        var loops = time * speed;
        var transformed = Vector2.Transform(StartPosition, Matrix3x2.CreateRotation(loops) * 2 * ((float)Math.PI));
        var scalingFactor = 1 - (LineSpacing + LineWidth) * loops / StartPosition.Length();
        transformed = Vector2.Transform(StartPosition, Matrix3x2.CreateScale(scalingFactor, scalingFactor));
        return transformed;
    }
}